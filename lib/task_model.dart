import 'package:flutter/material.dart';
import 'task.dart';
import 'task_provider.dart';

class TaskModel extends ChangeNotifier {

  TaskProvider _provider = TaskProvider();

  Task task;

  TaskModel.withProvider(this._provider);

  TaskModel();

  void update2(String code) async {
    task = await _provider.fetchTask(code);
    notifyListeners();
  }
}