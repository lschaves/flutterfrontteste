import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart';
import 'task.dart';
import 'package:http/io_client.dart';

class TaskProvider {

  Future<Task> fetchTask(String code) async {

    try {
      //Client endPoint = Client();
      final ioc = new HttpClient();
      ioc.badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
      final http = new IOClient(ioc);
      final response = await http.get('http://10.0.2.2:5000/todo/1');
      if (response.statusCode == 200) {
        return Task.fromJson(json.decode(response.body));
      } else {
        throw Exception('Failed to load owners for $code code.');
      }

    } on SocketException {
      print("SocketException: connection with the external server not found.");
    } catch(e) {
      print("Deu ruim: $e");
    }

  }
}
