import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'task_model.dart';

class Home extends StatefulWidget {
  Home({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController _controller = TextEditingController();

  void _buttonPressed() async {
    print('_buttonPressed');
    print(_controller.text);
    var model = Provider.of<TaskModel>(context, listen: false);
    model.update2(_controller.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                    labelText: 'TODO',
                    hintText: 'Task Title',
                    icon: Icon(Icons.check)),
                controller: _controller,
                key: ValueKey('field_task'),
              ),
              Consumer<TaskModel>(builder: (context, model, child) {
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.attach_file),
                          Text(model.task == null
                              ? ""
                              : "${model.task.title}"),
                        ],
                      ),
                    ],
                  ),
                );
              }),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        key: ValueKey('button_Find'),
        onPressed: _buttonPressed,
        tooltip: 'Consultar',
        child: Icon(Icons.add_circle),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}