

class Task {
  String _title;

  Task(this._title);

  Task.fromJson(Map<String, dynamic> parsedJson)
      : this(
            parsedJson['title']);

  String get title => _title;

  @override
  String toString() {
    return 'Task{_code: $_title }';
  }
}
